package com.example.anray.skillbranchtest.data;

import java.util.Map;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

import com.example.anray.skillbranchtest.data.House;
import com.example.anray.skillbranchtest.data.JoinPersonsAndHouses;
import com.example.anray.skillbranchtest.data.Person;

import com.example.anray.skillbranchtest.data.HouseDao;
import com.example.anray.skillbranchtest.data.JoinPersonsAndHousesDao;
import com.example.anray.skillbranchtest.data.PersonDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see org.greenrobot.greendao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig houseDaoConfig;
    private final DaoConfig joinPersonsAndHousesDaoConfig;
    private final DaoConfig personDaoConfig;

    private final HouseDao houseDao;
    private final JoinPersonsAndHousesDao joinPersonsAndHousesDao;
    private final PersonDao personDao;

    public DaoSession(Database db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        houseDaoConfig = daoConfigMap.get(HouseDao.class).clone();
        houseDaoConfig.initIdentityScope(type);

        joinPersonsAndHousesDaoConfig = daoConfigMap.get(JoinPersonsAndHousesDao.class).clone();
        joinPersonsAndHousesDaoConfig.initIdentityScope(type);

        personDaoConfig = daoConfigMap.get(PersonDao.class).clone();
        personDaoConfig.initIdentityScope(type);

        houseDao = new HouseDao(houseDaoConfig, this);
        joinPersonsAndHousesDao = new JoinPersonsAndHousesDao(joinPersonsAndHousesDaoConfig, this);
        personDao = new PersonDao(personDaoConfig, this);

        registerDao(House.class, houseDao);
        registerDao(JoinPersonsAndHouses.class, joinPersonsAndHousesDao);
        registerDao(Person.class, personDao);
    }
    
    public void clear() {
        houseDaoConfig.clearIdentityScope();
        joinPersonsAndHousesDaoConfig.clearIdentityScope();
        personDaoConfig.clearIdentityScope();
    }

    public HouseDao getHouseDao() {
        return houseDao;
    }

    public JoinPersonsAndHousesDao getJoinPersonsAndHousesDao() {
        return joinPersonsAndHousesDao;
    }

    public PersonDao getPersonDao() {
        return personDao;
    }

}
