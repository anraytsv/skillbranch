package com.example.anray.skillbranchtest.mvp.presenters;

import android.support.annotation.Nullable;

import com.example.anray.skillbranchtest.data.Person;
import com.example.anray.skillbranchtest.mvp.views.IListView;

import java.util.List;

/**
 * Created by anray on 24.10.2016.
 */

public interface IListPresenter {

    List<Person> getData(int page);

    void takeView(IListView listView);

    void dropView();

    void initView();

    @Nullable
    IListView getView();

}
