package com.example.anray.skillbranchtest;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.example.anray.skillbranchtest.data.House;
import com.example.anray.skillbranchtest.data.Person;
import com.example.anray.skillbranchtest.managers.ConstantManager;
import com.example.anray.skillbranchtest.managers.DataManager;
import com.example.anray.skillbranchtest.network.responses.PersonResponse;
import com.example.anray.skillbranchtest.network.responses.HouseResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by anray on 20.10.2016.
 */

public class LoaderNetToDb extends AsyncTaskLoader<Boolean> {
    private String TAG = "LoaderNetToDb";
    private DataManager mDataManager;


    private Call<HouseResponse> mCallHouse;
    private Call<PersonResponse> mCallCharacter;

    private boolean mIsFinished;

    public LoaderNetToDb(Context context) {
        super(context);
        DataManager.writeLog(TAG, "Loader constructor");

        mDataManager = DataManager.getInstance();


    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        DataManager.writeLog(TAG, "onStartLoading");
        if (mIsFinished != false) {
            DataManager.writeLog(TAG, "onStartLoading");
            deliverResult(mIsFinished);
        } else {
            forceLoad();
        }
    }

    @Override
    protected void onForceLoad() {
        super.onForceLoad();

        DataManager.writeLog(TAG, "onForceLoad start");
        DataManager.writeLog(TAG, "onForceLoad finished");

    }

    @Override
    public Boolean loadInBackground() {
        DataManager.writeLog(TAG, "loadInBackground start");

        long startTime =  new Date().getTime();

        if (mDataManager.getDaoSession().queryBuilder(Person.class).count() < 250) {


            //.where(UserDao.Properties.CodeLines.gt(0))
//                .orderDesc(UserDao.Properties.Rating)
//                .build()
//                .list();


            List<House> housesForDb = new ArrayList<>();
            List<Person> personsForDb = new ArrayList<>();

            List<String> characterUrls = new ArrayList<>();

            for (final int house : ConstantManager.houses) {
                characterUrls.clear();


                mCallHouse = mDataManager.getHouses(house);

                Response<HouseResponse> response = null;
                try {
                    response = mCallHouse.execute();
                    if (response.code() == 200) {
                        housesForDb.add(new House(house, ConstantManager.BASE_URL + "housesForDb/" + String.valueOf(house), response.body().getWords()));
                        characterUrls.addAll(response.body().getSwornMembers());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }


                //List<House> HousesOfPersons = new ArrayList<>();


                for (String characterUrl : characterUrls) {
                    mCallCharacter = mDataManager.getPersons(characterUrl);

                    Response<PersonResponse> response2 = null;
                    try {
                        response2 = mCallCharacter.execute();
                        if (response2.code() == 200) {

                            PersonResponse personResponse = response2.body();

                            Person person = new Person(personResponse, house);
                            //HousesOfPersons.addAll(person.getHousesCharacterLivesIn());
                            personsForDb.add(person);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            mDataManager.getDaoSession().getPersonDao().insertOrReplaceInTx(personsForDb);
            //mDataManager.getDaoSession().getHouseDao().insertOrReplaceInTx(HousesOfPersons);
            mDataManager.getDaoSession().getHouseDao().insertOrReplaceInTx(housesForDb);
        }

        while (new Date().getTime() - startTime < 3000){}

        mIsFinished = true;

        DataManager.writeLog(TAG, "loadInBackground finished");

        return mIsFinished;
    }

    @Override
    protected void onStopLoading() {
        DataManager.writeLog(TAG, "onStopLoading");
        if (mCallHouse != null) {
            mCallHouse.cancel();
        }

        if (mCallCharacter != null) {
            mCallCharacter.cancel();
        }
        super.onStopLoading();
    }

    @Override
    protected void onReset() {
        DataManager.writeLog(TAG, "onReset");
        super.onReset();
    }


}
