package com.example.anray.skillbranchtest.network;

import com.example.anray.skillbranchtest.managers.DataManager;
import com.example.anray.skillbranchtest.managers.PreferencesManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by anray on 20.10.2016.
 */

public class HeaderInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        PreferencesManager pm = DataManager.getInstance().getPreferencesManager();

        Request original = chain.request(); //оригинальный запрос, который шлем мы сейчас
        Request.Builder requestBuilder = original.newBuilder()
                .header("User-Agent", "GOTapp")
                .header("Cache-Control", "max-age="+(60*60*24));

        Request request = requestBuilder.build();

        return chain.proceed(request);
    }
}
