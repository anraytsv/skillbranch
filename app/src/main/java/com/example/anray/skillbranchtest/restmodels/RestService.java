package com.example.anray.skillbranchtest.restmodels;

import com.example.anray.skillbranchtest.network.responses.PersonResponse;
import com.example.anray.skillbranchtest.network.responses.HouseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Url;


/**
 * Created by anray on 06.10.2016.
 */

public interface RestService {

    @GET("houses/{number}")
    Call<HouseResponse> getHouses(@Path("number") int number);

    @GET
    Call<PersonResponse> getCharacters(@Url String url);
}
