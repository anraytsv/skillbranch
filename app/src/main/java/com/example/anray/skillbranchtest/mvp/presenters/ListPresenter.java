package com.example.anray.skillbranchtest.mvp.presenters;

import android.support.annotation.Nullable;

import com.example.anray.skillbranchtest.data.Person;
import com.example.anray.skillbranchtest.mvp.model.ListModel;
import com.example.anray.skillbranchtest.mvp.views.IListView;

import java.util.List;

/**
 * Created by anray on 24.10.2016.
 */

public class ListPresenter implements IListPresenter {
    private static ListPresenter ourInstance = null;

    private IListView mIListView;
    private ListModel mListModel;

    public ListPresenter() {
        mListModel = new ListModel(this);
    }

    public static ListPresenter getInstance() {
        if (ourInstance == null) {
            ourInstance = new ListPresenter();
        }
        return ourInstance;
    }


    @Override
    public List<Person> getData(int page) {

       return mListModel.getAllPersonsFromHouse(page);

    }

    @Override
    public void takeView(IListView listView) {
        mIListView = listView;
    }

    @Override
    public void dropView() {
        mIListView = null;
    }

    @Override
    public void initView() {

    }

    @Nullable
    @Override
    public IListView getView() {
        return mIListView;
    }
}
