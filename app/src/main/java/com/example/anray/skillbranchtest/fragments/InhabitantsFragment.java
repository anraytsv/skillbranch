package com.example.anray.skillbranchtest.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.anray.skillbranchtest.R;
import com.example.anray.skillbranchtest.activities.CharacterActivity;
import com.example.anray.skillbranchtest.adapters.PersonsAdapter;
import com.example.anray.skillbranchtest.data.Person;
import com.example.anray.skillbranchtest.mvp.presenters.ListPresenter;
import com.example.anray.skillbranchtest.mvp.views.IListView;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class InhabitantsFragment extends Fragment implements PersonsAdapter.CustomClickListener, IListView {
    private static final String FRAGMENT_POSITION = "FRAGMENT_POSITION";
    private int mPosition;

    private RecyclerView mRecyclerView;
    private List<Person> mPersons;

    private ListPresenter mListPresenter = ListPresenter.getInstance();

    public InhabitantsFragment() {
        // Required empty public constructor
    }


    public static InhabitantsFragment newInstance(int page) {
        InhabitantsFragment pageFragment = new InhabitantsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(FRAGMENT_POSITION, page);
        pageFragment.setArguments(bundle);

        return pageFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_inhabitants, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_persons);

        int page = getArguments().getInt(FRAGMENT_POSITION);

        mListPresenter.takeView(this);
        mListPresenter.initView();

        mPersons = mListPresenter.getData(page);
        placeDataInView(mPersons);


        return view;
    }

    @Override
    public void onDestroy() {
        mListPresenter.dropView();
        super.onDestroy();
    }

/*    // no more used after implementing MVP
    private List<Person> getAllPersonsFromHouse(int page) {

        return DataManager.getInstance().getDaoSession().getPersonDao().queryBuilder()
                .where(PersonDao.Properties.HouseId.eq(ConstantManager.houses[page]))
                .build()
                .list();

    }*/

    private void placeDataInView(List<Person> persons) {
        if (mRecyclerView != null) {
            LinearLayoutManager layoutmanager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(layoutmanager);
            mRecyclerView.setAdapter(new PersonsAdapter(persons, this));

        }
    }

    @Override
    public void onPersonClick(int position) {
        Intent personDetails = new Intent(getActivity(), CharacterActivity.class);
        personDetails.putExtra(CharacterActivity.CHARACTER_ID, mPersons.get(position).getCharacterId());
        startActivity(personDetails);
    }


}
