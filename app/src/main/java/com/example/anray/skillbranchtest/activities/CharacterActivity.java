package com.example.anray.skillbranchtest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.anray.skillbranchtest.R;
import com.example.anray.skillbranchtest.data.HouseDao;
import com.example.anray.skillbranchtest.data.Person;
import com.example.anray.skillbranchtest.data.PersonDao;
import com.example.anray.skillbranchtest.managers.ConstantManager;
import com.example.anray.skillbranchtest.managers.DataManager;
import com.example.anray.skillbranchtest.network.responses.PersonResponse;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CharacterActivity extends AppCompatActivity {

    public static final String CHARACTER_ID = "CHARACTER_ID";
    private static final String CHARACTER_DETAILS = "CHARACTER_DETAILS";
    private TextView mWords, mBorn, mTitles, mAliases;
    private Button mFather, mMother;
    private ImageView mGerb;
    private int mCharacterId;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);

        mWords = (TextView) findViewById(R.id.words);
        mBorn = (TextView) findViewById(R.id.born);
        mTitles = (TextView) findViewById(R.id.titles);
        mAliases = (TextView) findViewById(R.id.aliases);
        mFather = (Button) findViewById(R.id.father);
        mMother = (Button) findViewById(R.id.mother);
        mGerb = (ImageView) findViewById(R.id.gerb);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setupToolbar();


        Person person;
        mCharacterId = getIntent().getIntExtra(CHARACTER_ID, 0);
        if (mCharacterId != 0) {
            person = getPersonData(mCharacterId);
        } else {
            person = getIntent().getExtras().getParcelable(CHARACTER_DETAILS);
        }

        getSupportActionBar().setTitle(person.getName());


        mWords.setText(getWords(person.getHouseId()));
        if (!isNullOrEmpty(person.getBorn())) {
            mBorn.setText(person.getBorn());
        } else {
            findViewById(R.id.born_ly).setVisibility(View.GONE);
        }

        if (!isNullOrEmpty(person.getTitles())) {
            mTitles.setText(person.getTitles());
        } else {
            findViewById(R.id.titles_ly).setVisibility(View.GONE);
        }

        if (!isNullOrEmpty(person.getAliases())) {
            mAliases.setText(person.getAliases());
        } else {
            findViewById(R.id.aliases_ly).setVisibility(View.GONE);
        }

        if (!isNullOrEmpty(person.getFather())) {
            setParent(mFather, person.getFather(), person.getHouseId());
        } else {
            findViewById(R.id.father_ly).setVisibility(View.GONE);
        }

        if (!isNullOrEmpty(person.getMother())) {
            setParent(mMother, person.getMother(), person.getHouseId());
        } else {
            findViewById(R.id.mother_ly).setVisibility(View.GONE);
        }


        int drawable = R.drawable.stark;

        if (person.getHouseId() == ConstantManager.houses[0]) {
            drawable = R.drawable.stark;
        }
        if (person.getHouseId() == ConstantManager.houses[1]) {
            drawable = R.drawable.lannister;
        }
        if (person.getHouseId() == ConstantManager.houses[2]) {
            drawable = R.drawable.targarien;
        }

        Picasso.with(getApplicationContext())
                .load(drawable)
                //.resize(768, 512)
                .fit()
                .centerCrop()
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(mGerb);

        if (!isNullOrEmpty(person.getDied())) {
            String message;
            message = "Died " + person.getDied();

            if (!isNullOrEmpty(getLastSeason(person))) {
                message = message + "\nat " + getLastSeason(person);
            }


            Snackbar.make(mAliases, message, Snackbar.LENGTH_LONG).show();
        }


    }

    private boolean isNullOrEmpty(String str) {
        if (str != null && str.trim().length() != 0) {
            return false;
        }
        return true;
    }

    private void setParent(final Button view, String url, final int houseId) {

        Call<PersonResponse> houseCall = DataManager.getInstance().getPersons(url);
        houseCall.enqueue(new Callback<PersonResponse>() {
            @Override
            public void onResponse(Call<PersonResponse> call, final Response<PersonResponse> response) {
                if (response.code() == 200) {
                    String parentName = response.body().getName();
                    view.setText(parentName);
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent personDetails = new Intent(CharacterActivity.this, CharacterActivity.class);
                            personDetails.putExtra(CharacterActivity.CHARACTER_DETAILS, new Person(response.body(), houseId));
                            startActivity(personDetails);
                            //finish();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<PersonResponse> call, Throwable t) {

            }
        });

    }

    private String getLastSeason(Person person) {
        String tvSeries = person.getTvSeries();

        int index = tvSeries.lastIndexOf(",");
        tvSeries = tvSeries.substring(index + 1).trim();

        return tvSeries;
    }

    private String getWords(int houseId) {
        String words = "";

        try {
            words = DataManager.getInstance().getDaoSession().getHouseDao().queryBuilder()
                    .where(HouseDao.Properties.HouseId.eq(houseId))
                    .build()
                    .unique()
                    .getWords();
        } catch (Exception e) {
            e.printStackTrace();
            words = "";
        }

        return words;

    }


    private Person getPersonData(int characterId) {


        Person person;

        person = DataManager.getInstance().getDaoSession().getPersonDao().queryBuilder()
                .where(PersonDao.Properties.CharacterId.eq(characterId)).build().unique();

        return person;
    }

    private void setupToolbar() {

        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }
}
