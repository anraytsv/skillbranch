package com.example.anray.skillbranchtest.mvp.views;

/**
 * Created by anray on 24.10.2016.
 */

public interface ISplashView {

    void showMessage(String message);

    void showError(Throwable e);

    void showProgress();

    void hideProgress();
}
