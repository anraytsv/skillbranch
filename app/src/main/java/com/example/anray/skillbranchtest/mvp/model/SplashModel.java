package com.example.anray.skillbranchtest.mvp.model;

import android.os.AsyncTask;

import com.example.anray.skillbranchtest.data.House;
import com.example.anray.skillbranchtest.data.Person;
import com.example.anray.skillbranchtest.managers.ConstantManager;
import com.example.anray.skillbranchtest.managers.DataManager;
import com.example.anray.skillbranchtest.mvp.presenters.SplashPresenter;
import com.example.anray.skillbranchtest.network.responses.HouseResponse;
import com.example.anray.skillbranchtest.network.responses.PersonResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by anray on 24.10.2016.
 */

public class SplashModel {

    private DataManager mDataManager = DataManager.getInstance();
    private SplashPresenter mSplashPresenter;

    public SplashModel(SplashPresenter splashPresenter) {
        mSplashPresenter = splashPresenter;
    }

    private void loadDataFromNet() {


    }

    public boolean hasDbData() {

        if (mDataManager.getDaoSession().queryBuilder(Person.class).count() > 250) {
            return true;
        }
        return false;
    }

    private void loadDataFromDb() {

    }

    public void getAllData() {

        if (hasDbData() == false) {
            new MyTask().execute();
        } else {
            mSplashPresenter.openMainScreen();
        }


    }

    class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            List<House> housesForDb = new ArrayList<>();
            List<Person> personsForDb = new ArrayList<>();

            List<String> characterUrls = new ArrayList<>();

            Call<HouseResponse> callHouse;
            Call<PersonResponse> callCharacter;

            for (final int house : ConstantManager.houses) {
                characterUrls.clear();


                callHouse = mDataManager.getHouses(house);

                Response<HouseResponse> response = null;
                try {
                    response = callHouse.execute();
                    if (response.code() == 200) {
                        housesForDb.add(new House(house, ConstantManager.BASE_URL + "housesForDb/" + String.valueOf(house), response.body().getWords()));
                        characterUrls.addAll(response.body().getSwornMembers());
                    }
                } catch (IOException e) {
                    mSplashPresenter.getView().showError(e);
                }


                for (String characterUrl : characterUrls) {
                    callCharacter = mDataManager.getPersons(characterUrl);

                    Response<PersonResponse> response2 = null;
                    try {
                        response2 = callCharacter.execute();
                        if (response2.code() == 200) {

                            PersonResponse personResponse = response2.body();

                            Person person = new Person(personResponse, house);
                            personsForDb.add(person);
                        }
                    } catch (IOException e) {
                        mSplashPresenter.getView().showError(e);
                    }

                }
            }

            mDataManager.getDaoSession().getPersonDao().insertOrReplaceInTx(personsForDb);
            mDataManager.getDaoSession().getHouseDao().insertOrReplaceInTx(housesForDb);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mSplashPresenter.openMainScreen();
        }
    }
}
