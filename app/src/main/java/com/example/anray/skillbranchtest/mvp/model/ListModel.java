package com.example.anray.skillbranchtest.mvp.model;

import com.example.anray.skillbranchtest.data.Person;
import com.example.anray.skillbranchtest.data.PersonDao;
import com.example.anray.skillbranchtest.managers.ConstantManager;
import com.example.anray.skillbranchtest.managers.DataManager;
import com.example.anray.skillbranchtest.mvp.presenters.ListPresenter;

import java.util.List;

/**
 * Created by anray on 24.10.2016.
 */

public class ListModel {
    private DataManager mDataManager = DataManager.getInstance();
    private ListPresenter mListPresenter;
    private List<Person> mPersons1;
    private List<Person> mPersons2;
    private List<Person> mPersons3;

    public ListModel(ListPresenter listPresenter) {
        mListPresenter = listPresenter;
    }


    public List<Person> getAllPersonsFromHouse(int page) {

        switch (page) {
            case 0:
                if (mPersons1 == null) {
                    mPersons1 = DataManager.getInstance().getDaoSession().getPersonDao().queryBuilder()
                            .where(PersonDao.Properties.HouseId.eq(ConstantManager.houses[page]))
                            .build()
                            .list();
                }
                return mPersons1;
            case 1:
                if (mPersons2 == null) {
                    mPersons2 = DataManager.getInstance().getDaoSession().getPersonDao().queryBuilder()
                            .where(PersonDao.Properties.HouseId.eq(ConstantManager.houses[page]))
                            .build()
                            .list();
                }
                return mPersons2;
            case 2:
                if (mPersons3 == null) {
                    mPersons3 = DataManager.getInstance().getDaoSession().getPersonDao().queryBuilder()
                            .where(PersonDao.Properties.HouseId.eq(ConstantManager.houses[page]))
                            .build()
                            .list();
                }
                return mPersons3;
            default:
                return DataManager.getInstance().getDaoSession().getPersonDao().queryBuilder()
                        .where(PersonDao.Properties.HouseId.eq(ConstantManager.houses[page]))
                        .build()
                        .list();
        }


    }


}
