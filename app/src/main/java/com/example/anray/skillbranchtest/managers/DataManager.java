package com.example.anray.skillbranchtest.managers;

import android.content.Context;
import android.util.Log;

import com.example.anray.skillbranchtest.App;
import com.example.anray.skillbranchtest.data.DaoSession;
import com.example.anray.skillbranchtest.network.responses.PersonResponse;
import com.example.anray.skillbranchtest.network.responses.HouseResponse;
import com.example.anray.skillbranchtest.restmodels.RestService;
import com.example.anray.skillbranchtest.restmodels.ServiceGenerator;
import com.squareup.picasso.Picasso;

import retrofit2.Call;

/**
 * Created by anray on 20.10.2016.
 */

public class DataManager {
    private static DataManager INSTANCE = null;

    private PreferencesManager mPreferencesManager;
    private RestService mRestService;

    private Context mContext;

    private Picasso mPicasso;
    private DaoSession mDaoSession;

    private DataManager() {
        this.mPreferencesManager = new PreferencesManager();
        this.mRestService = ServiceGenerator.createService(RestService.class);
        this.mContext = App.getContext();
        this.mRestService = ServiceGenerator.createService(RestService.class);
        this.mDaoSession = App.getDaoSession();
    }

    public Context getContext() {
        return mContext;
    }


    public DaoSession getDaoSession() {
        return mDaoSession;
    }

    public static DataManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DataManager();

        }
        return INSTANCE;
    }

    public static void writeLog(Object where, Object message) {

        if (ConstantManager.DEBUG == true) {
            Log.d(ConstantManager.TAG_PREFIX, where.toString() + ": " + message.toString());
        }

    }

    public static void writeLog(Object message) {

        if (ConstantManager.DEBUG == true) {
            Log.d(ConstantManager.TAG_PREFIX, message.toString());
        }

    }



    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    public Call<HouseResponse> getHouses(int number) {
        return mRestService.getHouses(number);
    }

    public Call<PersonResponse> getPersons(String url) {
        return mRestService.getCharacters(url);
    }

}

