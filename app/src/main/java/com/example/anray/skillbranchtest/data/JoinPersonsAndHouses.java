package com.example.anray.skillbranchtest.data;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by anray on 20.10.2016.
 */
@Entity
public class JoinPersonsAndHouses {
    @Id
    private Long id;
    private Long characterId;
    private Long houseId;
    @Generated(hash = 677864852)
    public JoinPersonsAndHouses(Long id, Long characterId, Long houseId) {
        this.id = id;
        this.characterId = characterId;
        this.houseId = houseId;
    }
    @Generated(hash = 305749324)
    public JoinPersonsAndHouses() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getCharacterId() {
        return this.characterId;
    }
    public void setCharacterId(Long characterId) {
        this.characterId = characterId;
    }
    public Long getHouseId() {
        return this.houseId;
    }
    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }

}
