package com.example.anray.skillbranchtest.activities;

import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.Loader;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.anray.skillbranchtest.BuildConfig;
import com.example.anray.skillbranchtest.LoaderNetToDb;
import com.example.anray.skillbranchtest.R;
import com.example.anray.skillbranchtest.managers.DataManager;
import com.example.anray.skillbranchtest.mvp.presenters.SplashPresenter;
import com.example.anray.skillbranchtest.mvp.views.ISplashView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Boolean>, ISplashView {

    private static final String TAG = "SplashActivity";
    @BindView(R.id.activity_splash)
    LinearLayout mLinearLayout;
    private ProgressDialog mProgressDialog;
    private SplashPresenter mSplashPresenter = SplashPresenter.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);*/

        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        //getLoaderManager().initLoader(0, null, this);

        mSplashPresenter.takeView(this);
        mSplashPresenter.initView();


    }

    @Override
    protected void onDestroy() {
        mSplashPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public Loader<Boolean> onCreateLoader(int id, Bundle args) {
        if (id == 0) {
            showProgress();
            return new LoaderNetToDb(this);
        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<Boolean> loader, Boolean data) {

        DataManager.writeLog(TAG, "onLoadFinished finished");
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        hideProgress();
        finish();

    }

    @Override
    public void onLoaderReset(Loader<Boolean> loader) {

    }

    @Override
    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(this, null, null);
            mProgressDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mProgressDialog.setContentView(new ProgressBar(this));



        } else {
            mProgressDialog.show();

        }

    }
    @Override
    public void hideProgress() {
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }

    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(mLinearLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.something_went_wrong_message));
            // TODO: 23.10.2016 send error details to crashlytics
        }
    }
}
