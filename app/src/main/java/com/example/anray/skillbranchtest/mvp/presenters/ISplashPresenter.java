package com.example.anray.skillbranchtest.mvp.presenters;

import android.support.annotation.Nullable;

import com.example.anray.skillbranchtest.mvp.views.ISplashView;

/**
 * Created by anray on 24.10.2016.
 */

public interface ISplashPresenter {



    void openMainScreen();

    void takeView(ISplashView splashView);

    void dropView();

    void initView();

    @Nullable
    ISplashView getView();

    void getData();


}
