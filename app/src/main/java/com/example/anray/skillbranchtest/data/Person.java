package com.example.anray.skillbranchtest.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.example.anray.skillbranchtest.network.responses.PersonResponse;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinEntity;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.Unique;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anray on 20.10.2016.
 */
@Entity(active = true, nameInDb = "CHARACTERS")
public class Person implements Parcelable {

    @Id
    private Long id;

    @NotNull
    @Unique
    private int characterId;

    @NotNull
    @Unique
    private String url;
    private int houseId;
    private String born;
    private String died;
    private String father;
    private String mother;
    private String name;

    //combined from List<String> with ", "
    private String titles;

    //combined from List<String> with ", "
    private String aliases;

    //combined from List<String> with ", "
    private String tvSeries;

    @ToMany
    @JoinEntity(
            entity = JoinPersonsAndHouses.class,
            sourceProperty = "characterId",
            targetProperty = "houseId"
    )
    private List<House> housesCharacterLivesIn;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 778611619)
    private transient PersonDao myDao;

    public Person(PersonResponse personResponse, int house) {
        String url = personResponse.getUrl();

        this.houseId = house;
        this.characterId = Integer.parseInt(url.split("characters/")[1].trim());
        this.url = url;
        this.born = personResponse.getBorn();
        this.died = personResponse.getDied();
        this.father = personResponse.getFather();
        this.mother = personResponse.getMother();
        this.name = personResponse.getName();
        this.titles = TextUtils.join(", ", personResponse.getTitles());
        this.aliases = TextUtils.join(", ", personResponse.getAliases());
        this.tvSeries = TextUtils.join(", ", personResponse.getTvSeries());

        List<House> houses = new ArrayList<>();
        for (String s : personResponse.getAllegiances()) {
            houses.add(new House(Integer.parseInt(s.split("houses/")[1].trim()),s,""));
        }

        this.housesCharacterLivesIn = houses;
    }

    @Generated(hash = 1913352324)
    public Person(Long id, int characterId, @NotNull String url, int houseId, String born, String died,
                  String father, String mother, String name, String titles, String aliases, String tvSeries) {
        this.id = id;
        this.characterId = characterId;
        this.url = url;
        this.houseId = houseId;
        this.born = born;
        this.died = died;
        this.father = father;
        this.mother = mother;
        this.name = name;
        this.titles = titles;
        this.aliases = aliases;
        this.tvSeries = tvSeries;
    }

    @Generated(hash = 1024547259)
    public Person() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCharacterId() {
        return this.characterId;
    }

    public void setCharacterId(int characterId) {
        this.characterId = characterId;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBorn() {
        return this.born;
    }

    public void setBorn(String born) {
        this.born = born;
    }

    public String getDied() {
        return this.died;
    }

    public void setDied(String died) {
        this.died = died;
    }

    public String getFather() {
        return this.father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getMother() {
        return this.mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitles() {
        return this.titles;
    }

    public void setTitles(String titles) {
        this.titles = titles;
    }

    public String getAliases() {
        return this.aliases;
    }

    public void setAliases(String aliases) {
        this.aliases = aliases;
    }

    public String getTvSeries() {
        return this.tvSeries;
    }

    public void setTvSeries(String tvSeries) {
        this.tvSeries = tvSeries;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 276695283)
    public List<House> getHousesCharacterLivesIn() {
        if (housesCharacterLivesIn == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            HouseDao targetDao = daoSession.getHouseDao();
            List<House> housesCharacterLivesInNew = targetDao
                    ._queryPerson_HousesCharacterLivesIn(id);
            synchronized (this) {
                if (housesCharacterLivesIn == null) {
                    housesCharacterLivesIn = housesCharacterLivesInNew;
                }
            }
        }
        return housesCharacterLivesIn;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 2043112010)
    public synchronized void resetHousesCharacterLivesIn() {
        housesCharacterLivesIn = null;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 2056799268)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getPersonDao() : null;
    }

    public int getHouseId() {
        return this.houseId;
    }

    public void setHouseId(int houseId) {
        this.houseId = houseId;
    }

    protected Person(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readLong();
        characterId = in.readInt();
        url = in.readString();
        houseId = in.readInt();
        born = in.readString();
        died = in.readString();
        father = in.readString();
        mother = in.readString();
        name = in.readString();
        titles = in.readString();
        aliases = in.readString();
        tvSeries = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(id);
        }
        dest.writeInt(characterId);
        dest.writeString(url);
        dest.writeInt(houseId);
        dest.writeString(born);
        dest.writeString(died);
        dest.writeString(father);
        dest.writeString(mother);
        dest.writeString(name);
        dest.writeString(titles);
        dest.writeString(aliases);
        dest.writeString(tvSeries);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Person> CREATOR = new Parcelable.Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };
}