package com.example.anray.skillbranchtest.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.anray.skillbranchtest.R;
import com.example.anray.skillbranchtest.data.Person;
import com.example.anray.skillbranchtest.managers.ConstantManager;
import com.example.anray.skillbranchtest.managers.DataManager;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PersonsAdapter extends RecyclerView.Adapter<PersonsAdapter.PersonViewHolder> {

    private static final String TAG = ConstantManager.TAG_PREFIX + "UsersAdapter";
    private DataManager mDataManager;
    private List<Person> mPersons;
    private Context mContext;
    private PersonsAdapter.CustomClickListener mCustomClickListener;


    public PersonsAdapter(List<Person> persons, PersonsAdapter.CustomClickListener customClickListener) {
        mPersons = persons;
        this.mCustomClickListener = customClickListener;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.person_layout_for_rv, parent, false);


        return new PersonViewHolder(convertView, mCustomClickListener);
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder holder, int position) {

        mDataManager = DataManager.getInstance();
        final Person person = mPersons.get(position);

        int drawable = R.drawable.stark_icon;

        if (person.getHouseId() == ConstantManager.houses[0]) {
            drawable = R.drawable.stark_icon;
        }
        if (person.getHouseId() == ConstantManager.houses[1]) {
            drawable = R.drawable.lanister_icon;
        }
        if (person.getHouseId() == ConstantManager.houses[2]) {
            drawable = R.drawable.targarien_icon;
        }


        //holder.mIcon.setImageResource(drawable);

        Picasso.with(mContext)
                .load(drawable)
                //.resize(768, 512)
                .fit()
                .centerCrop()
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.mIcon);

        holder.mName.setText(person.getName());
        holder.mTitle.setText(person.getTitles());

    }

    @Override
    public int getItemCount() {
        return mPersons.size();
    }

    public interface CustomClickListener {

        void onPersonClick(int position);
    }

    public static class PersonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView mName, mTitle;

        protected ImageView mIcon;

        private CustomClickListener mListener;


        public PersonViewHolder(View itemView, CustomClickListener customClickListener) {
            super(itemView);
            this.mListener = customClickListener;

            mName = (TextView) itemView.findViewById(R.id.name);
            mTitle = (TextView) itemView.findViewById(R.id.title);

            mIcon = (ImageView) itemView.findViewById(R.id.icon);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onPersonClick(getAdapterPosition());
            }

        }
    }


}



