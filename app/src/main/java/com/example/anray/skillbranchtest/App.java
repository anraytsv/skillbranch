package com.example.anray.skillbranchtest;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.anray.skillbranchtest.data.DaoMaster;
import com.example.anray.skillbranchtest.data.DaoSession;
import com.facebook.stetho.Stetho;

import org.greenrobot.greendao.database.Database;

/**
 * Created by anray on 20.10.2016.
 */

public class App extends android.app.Application {
    private static Context sContext;

    private static SharedPreferences sSharedPreferences;
    private static DaoSession sDaoSession;

    public static DaoSession getDaoSession() {
        return sDaoSession;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sContext = this;


        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "GOT-db");
        Database db = helper.getWritableDb();
        sDaoSession = new DaoMaster(db).newSession();

        Stetho.initializeWithDefaults(this);

    }

    public static Context getContext() {
        return sContext;
    }

    public static SharedPreferences getSharedPreferences() {

        return sSharedPreferences;
    }
}
