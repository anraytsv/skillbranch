package com.example.anray.skillbranchtest.activities;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.example.anray.skillbranchtest.R;
import com.example.anray.skillbranchtest.fragments.InhabitantsFragment;

public class MainActivity extends AppCompatActivity {

    private static final int PAGE_COUNT = 3;

    private ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;
    private TabLayout mTabLayout;
    private Toolbar mToolbar;
    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setupToolbar();

        mTabLayout = (TabLayout) findViewById(R.id.tabs);


        mViewPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                changeMainColor(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);

            actionBar.setDisplayHomeAsUpEnabled(true);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupDrawer();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //инициализация аватарки
        ImageView mAvatar = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.avatar);


        if (item.getItemId() == android.R.id.home) {
            mDrawerLayout.openDrawer(GravityCompat.START);
            switch (mViewPager.getCurrentItem()){
                case 0:
                    mNavigationView.getMenu().findItem(R.id.starks).setChecked(true);
                    mAvatar.setImageResource(R.drawable.stark_icon);
                    break;
                case 1:
                    mNavigationView.getMenu().findItem(R.id.lannister).setChecked(true);
                    mAvatar.setImageResource(R.drawable.lanister_icon);
                    break;
                case 2:
                    mNavigationView.getMenu().findItem(R.id.tangaryer).setChecked(true);
                    mAvatar.setImageResource(R.drawable.targarien_icon);
                    break;
            }
        }


        return super.onOptionsItemSelected(item);
    }

    private void setupDrawer() {



        //устанавливает выбранным пункт меню, где мы находимся





        mNavigationView.setNavigationItemSelectedListener(new NavigationView
                .OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                //showSnackbar(item.getTitle().toString());

                switch (item.getItemId()) {
                    case R.id.starks:
                        item.setChecked(true);
                        mViewPager.setCurrentItem(0);

                        break;
                    case R.id.lannister:
                        item.setChecked(true);
                        mViewPager.setCurrentItem(1);
                        break;
                    case R.id.tangaryer:
                        item.setChecked(true);
                        mViewPager.setCurrentItem(2);
                        break;
                }


                mDrawerLayout.closeDrawer(GravityCompat.START);

                return false;
            }
        });
    }

    private class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            return InhabitantsFragment.newInstance(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = "";

            switch (position) {
                case 0:
                    title = getString(R.string.starks);
                    return title;
                case 1:
                    title = getString(R.string.lannister);
                    return title;
                case 2:
                    title = getString(R.string.tangaryer);
                    return title;
            }
            return super.getPageTitle(position);

        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

    }

    private void changeMainColor(int position) {
        int color = R.color.colorPrimary;
        switch (position) {
            case 0:
                color = R.color.color_gray;

                break;
            case 1:
                color = R.color.colorPrimary;
                //this.setTheme(R.style.AppTheme2);
                break;
            case 2:
                color = R.color.cardview_dark_background;
                //this.setTheme(R.style.AppTheme2);

                break;
        }
        /*Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= 21) {
            window.setStatusBarColor(ContextCompat.getColor(this, color));
            window.setNavigationBarColor(color);
        }*/
    }
}
