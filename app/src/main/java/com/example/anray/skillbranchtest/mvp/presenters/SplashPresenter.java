package com.example.anray.skillbranchtest.mvp.presenters;

import android.content.Intent;
import android.support.annotation.Nullable;

import com.example.anray.skillbranchtest.activities.MainActivity;
import com.example.anray.skillbranchtest.activities.SplashActivity;
import com.example.anray.skillbranchtest.mvp.model.SplashModel;
import com.example.anray.skillbranchtest.mvp.views.ISplashView;

import java.util.Date;

/**
 * Created by anray on 24.10.2016.
 */

public class SplashPresenter implements ISplashPresenter {
    private static SplashPresenter ourInstance = null;

    private ISplashView mSplashView;
    private SplashModel mSplashModel;

    public SplashPresenter() {
        mSplashModel = new SplashModel(this);
    }

    public static SplashPresenter getInstance() {
        if (ourInstance == null) {
            ourInstance = new SplashPresenter();
        }
        return ourInstance;
    }

    @Override
    public void getData() {

    }

    @Override
    public void openMainScreen() {
        if (getView() != null) {
            Intent intent = new Intent(((SplashActivity) getView()), MainActivity.class);
            ((SplashActivity) getView()).startActivity(intent);
            ((SplashActivity) getView()).finish();
        }
    }

    @Override
    public void takeView(ISplashView splashView) {
        mSplashView = splashView;
    }

    @Override
    public void dropView() {
        mSplashView = null;
    }

    @Override
    public void initView() {
        if (getView() != null) {
            getView().showProgress();
            long startTime = new Date().getTime();
            mSplashModel.getAllData();
            while (new Date().getTime() - startTime < 3000) {
            }
            getView().hideProgress();
        }
    }

    @Nullable
    @Override
    public ISplashView getView() {
        return mSplashView;
    }
}
